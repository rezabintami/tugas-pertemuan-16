package mahasiswa

import(
	"context"
	"fmt"
	"log"
	"time"
	"database/sql"
	"errors"
	
	"projectku/config"
	"projectku/models"
)

const(
	table			= "mahasiswa"
	layoutDateTime 	= "2006-01-02 15:04:05"
)

func GetAll(ctx context.Context) ([]models.Mahasiswa, error)  {
	var mahasiswas []models.Mahasiswa

	db, err := config.MySQL()
	if err != nil{
		log.Fatal("Cant connect to MySQL",err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v Order by id DESC", table)

	rowQuery, err := db.QueryContext(ctx,queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next(){
		var mahasiswa models.Mahasiswa
		var createdAt, updatedAt string
		if err = rowQuery.Scan(
			&mahasiswa.ID,
			&mahasiswa.NIM,
			&mahasiswa.Name,
			&mahasiswa.Semester,
			&createdAt,
			&updatedAt);
		err != nil{
			return nil, err
		}

		mahasiswa.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
		if err != nil{
			log.Fatal(err)
		}
		mahasiswa.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		if err != nil{
			log.Fatal(err)
		}

		mahasiswas = append(mahasiswas, mahasiswa)
	}
	return mahasiswas, nil
}

func Insert(ctx context.Context, mhs models.Mahasiswa) error {
	db,err := config.MySQL()

	if err != nil{
		log.Fatal("Cant Connect to MySQL",err)
	}

	queryText := fmt.Sprintf("INSERT INTO %v (nim,name,semester,update,created_at, updated_at) values(%v,'%v',%v,%v,'%v','%v')", table,
	mhs.NIM,
	mhs.Name,
	mhs.Semester,
	0,
	time.Now().Format(layoutDateTime),
	time.Now().Format(layoutDateTime))

	_, err = db.ExecContext(ctx,queryText)

	if err != nil{
		return err
	}
	return nil
}

func Update(ctx context.Context, mhs models.Mahasiswa) ([]models.Mahasiswa, error){

	var mahasiswas []models.Mahasiswa

	db,err := config.MySQL()
	
	if err != nil{
		log.Fatal("Cant Connect to MySQL",err)
	}

	query := fmt.Sprintf("SELECT * from %v where id = '%d'", table,mhs.ID)

	rowQuery, err := db.QueryContext(ctx,query)
	if err != nil {
		log.Fatal(err)
	}
	for rowQuery.Next(){
		var mahasiswa models.Mahasiswa
		var createdAt, updatedAt string
		if err = rowQuery.Scan(
			&mahasiswa.ID,
			&mahasiswa.NIM,
			&mahasiswa.Name,
			&mahasiswa.Semester,
			&mahasiswa.Update,
			&createdAt,
			&updatedAt);
		err != nil{
			return nil, err
		}
		fmt.Println(4)
		mahasiswa.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
		if err != nil{
			log.Fatal(err)
		}
		fmt.Println(5)
		mahasiswa.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		if err != nil{
			log.Fatal(err)
		}
		fmt.Println(6)
		mahasiswas = append(mahasiswas, mahasiswa)
	}
	if mahasiswas == nil {
		return nil,errors.New("ID Tidak Ditemukan")
	}
	sum := mahasiswas[0].Update + 1

	queryText := fmt.Sprintf("UPDATE %v set nim = %d, name='%s', semester = %d, sum_updated = '%d', updated_at = '%v' where id = '%d'", table,
	mhs.NIM,
	mhs.Name,
	mhs.Semester,
	sum,
	time.Now().Format(layoutDateTime),
	mhs.ID)
	s, err := db.ExecContext(ctx,queryText)
	if err != nil{
		return nil,err
	}
	check, err := s.RowsAffected()
	if check == 0 {
		return nil,errors.New("ID Tidak Ditemukan")
	}
	mahasiswas[0].Update = sum
	return mahasiswas,nil
}

func Delete(ctx context.Context, mhs models.Mahasiswa) ([]models.Mahasiswa, error) {
	var mahasiswas []models.Mahasiswa

	db,err := config.MySQL()
	
	if err != nil{
		log.Fatal("Cant Connect to MySQL",err)
	}
	query := fmt.Sprintf("SELECT * from %v where id = '%d'", table,mhs.ID)

	rowQuery, err := db.QueryContext(ctx,query)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next(){
		var mahasiswa models.Mahasiswa
		var createdAt, updatedAt string
		if err = rowQuery.Scan(
			&mahasiswa.ID,
			&mahasiswa.NIM,
			&mahasiswa.Name,
			&mahasiswa.Semester,
			&mahasiswa.Update,
			&createdAt,
			&updatedAt);
		err != nil{
			return nil, err
		}

		mahasiswa.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
		if err != nil{
			log.Fatal(err)
		}
		mahasiswa.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
		if err != nil{
			log.Fatal(err)
		}

		mahasiswas = append(mahasiswas, mahasiswa)
	}
	if mahasiswas == nil {
		return nil,errors.New("ID Tidak Ditemukan")
	}
	queryText := fmt.Sprintf("DELETE from %v where id = '%d'", table,mhs.ID)
	
	s, err := db.ExecContext(ctx,queryText)

	if err != nil && err != sql.ErrNoRows{
		return nil,err
	}

	check, err := s.RowsAffected()
	if check == 0 {
		return nil,errors.New("id tidak ada")
	}
	return mahasiswas,nil
}
